# Check the [Wiki](https://bitbucket.org/shiyu_liang/cc3200_quad_hardware/wiki/Home) for description of this project #

| List of directories |            |
| ------------- |:-------------:|
| docs | various documentations used through the schematics |
| lib | KiCad Schematic and Footprint library in one convenient folder|
| main | rev 1.0 of main control board *DOES NOT WORK*|
| main2.0 | rev 2.0 of main control board, slightly larger BUT functional|
| mount | SolidWorks design files for motor mounts for 8.5mm x 20mm DC motor|
| wings | 3-pin connector to main board, acts as extensions to motors |
| wings2.0 | 2-pin connector to main board, acts as extensions to motors |