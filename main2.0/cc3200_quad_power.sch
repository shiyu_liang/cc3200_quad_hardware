EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:cc3200_quad
LIBS:dips-s
LIBS:cc3200_quad-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BATTERY BT1
U 1 1 55257CF7
P 4450 3350
F 0 "BT1" H 4450 3550 50  0000 C CNN
F 1 "JST_CONNECTOR" H 4450 3160 50  0000 C CNN
F 2 "cc3200_quad_footprints:JST_2PIN_2mm" H 4450 3350 60  0001 C CNN
F 3 "" H 4450 3350 60  0000 C CNN
	1    4450 3350
	0    1    1    0   
$EndComp
$Comp
L GND #PWR069
U 1 1 55257D33
P 4450 3850
F 0 "#PWR069" H 4450 3850 30  0001 C CNN
F 1 "GND" H 4450 3780 30  0001 C CNN
F 2 "" H 4450 3850 60  0000 C CNN
F 3 "" H 4450 3850 60  0000 C CNN
	1    4450 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3650 4450 3850
$Comp
L +3.3V #PWR070
U 1 1 55258B05
P 6350 3000
F 0 "#PWR070" H 6350 2960 30  0001 C CNN
F 1 "+3.3V" H 6350 3110 30  0000 C CNN
F 2 "" H 6350 3000 60  0000 C CNN
F 3 "" H 6350 3000 60  0000 C CNN
	1    6350 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3050 6350 3000
$Comp
L CP2 C27
U 1 1 55259D90
P 6250 3250
F 0 "C27" H 6250 3350 40  0000 L CNN
F 1 "100uF" H 6256 3165 40  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeA_EIA-3216_Reflow" H 6288 3100 30  0001 C CNN
F 3 "" H 6250 3250 60  0000 C CNN
	1    6250 3250
	1    0    0    -1  
$EndComp
$Comp
L CP2 C26
U 1 1 55259DDF
P 4900 3300
F 0 "C26" H 4900 3400 40  0000 L CNN
F 1 "100uF" H 4906 3215 40  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeA_EIA-3216_Reflow" H 4938 3150 30  0001 C CNN
F 3 "" H 4900 3300 60  0000 C CNN
	1    4900 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR071
U 1 1 55259F20
P 4900 3550
F 0 "#PWR071" H 4900 3550 30  0001 C CNN
F 1 "GND" H 4900 3480 30  0001 C CNN
F 2 "" H 4900 3550 60  0000 C CNN
F 3 "" H 4900 3550 60  0000 C CNN
	1    4900 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3500 4900 3550
$Comp
L +BATT #PWR072
U 1 1 552604FE
P 4450 3050
F 0 "#PWR072" H 4450 3000 20  0001 C CNN
F 1 "+BATT" H 4450 3150 30  0000 C CNN
F 2 "" H 4450 3050 60  0000 C CNN
F 3 "" H 4450 3050 60  0000 C CNN
	1    4450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3050 5150 3050
Wire Wire Line
	4900 3050 4900 3100
Connection ~ 4900 3050
$Comp
L GND #PWR073
U 1 1 55294A08
P 5050 3250
F 0 "#PWR073" H 5050 3250 30  0001 C CNN
F 1 "GND" H 5050 3180 30  0001 C CNN
F 2 "" H 5050 3250 60  0000 C CNN
F 3 "" H 5050 3250 60  0000 C CNN
	1    5050 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3150 5050 3250
$Comp
L GND #PWR074
U 1 1 55294ACA
P 6250 3500
F 0 "#PWR074" H 6250 3500 30  0001 C CNN
F 1 "GND" H 6250 3430 30  0001 C CNN
F 2 "" H 6250 3500 60  0000 C CNN
F 3 "" H 6250 3500 60  0000 C CNN
	1    6250 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3050 6350 3050
Connection ~ 6250 3050
Wire Wire Line
	6250 3450 6250 3500
$Comp
L AP7363 U7
U 1 1 5555179E
P 5550 3150
F 0 "U7" H 5550 3150 60  0000 C CNN
F 1 "AP7363" H 5550 3350 60  0000 C CNN
F 2 "cc3200_quad_footprints:AP7363-TO252-3" H 5550 3150 60  0001 C CNN
F 3 "" H 5550 3150 60  0000 C CNN
	1    5550 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3150 5150 3150
Wire Wire Line
	5150 3150 5150 3250
$EndSCHEMATC
