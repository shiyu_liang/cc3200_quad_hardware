EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:wings-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X02 P2
U 1 1 555D7118
P 5800 3550
F 0 "P2" H 5800 3700 50  0000 C CNN
F 1 "toMotor" V 5900 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 5800 3550 60  0001 C CNN
F 3 "" H 5800 3550 60  0000 C CNN
	1    5800 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3500 5600 3500
Wire Wire Line
	5300 3600 5600 3600
$Comp
L CONN_01X03 P1
U 1 1 55A3D6EB
P 5100 3600
F 0 "P1" H 5100 3800 50  0000 C CNN
F 1 "CONN_01X03" V 5200 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 5100 3600 60  0001 C CNN
F 3 "" H 5100 3600 60  0000 C CNN
	1    5100 3600
	-1   0    0    1   
$EndComp
$EndSCHEMATC
