#!/usr/bin/python

import sys
import xml.etree.ElementTree as ET

tree = ET.parse(sys.argv[1])
root = tree.getroot()

comps = {}

for comp in root.iter('comp'):
	val = comp.find('value').text
	ft = comp.find('footprint').text
	key = ft+','+val
	if key in comps:
		comps[key] += 1
	else:
		comps[key] = 1

for k, val in comps.iteritems():
	print k+',', val
