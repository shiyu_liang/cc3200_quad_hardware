EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:cc3200_quad
LIBS:dips-s
LIBS:cc3200_quad-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DC_Motor U3
U 1 1 552FEC3E
P 6150 3850
F 0 "U3" H 6150 3700 60  0000 C CNN
F 1 "DC_Motor" H 6150 3600 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6150 3850 60  0001 C CNN
F 3 "" H 6150 3850 60  0000 C CNN
	1    6150 3850
	0    1    1    0   
$EndComp
$Comp
L DIODE D1
U 1 1 552FECD1
P 6400 3800
F 0 "D1" H 6400 3900 40  0000 C CNN
F 1 "BAT54-7-F" H 6400 3700 40  0000 C CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT-23" H 6400 3800 60  0001 C CNN
F 3 "" H 6400 3800 60  0000 C CNN
	1    6400 3800
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 552FEEC8
P 5600 5100
F 0 "R2" V 5680 5100 40  0000 C CNN
F 1 "1.5k" V 5607 5101 40  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 5530 5100 30  0001 C CNN
F 3 "" H 5600 5100 30  0000 C CNN
	1    5600 5100
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 552FEEF3
P 5200 4750
F 0 "R1" V 5280 4750 40  0000 C CNN
F 1 "150" V 5207 4751 40  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 5130 4750 30  0001 C CNN
F 3 "" H 5200 4750 30  0000 C CNN
	1    5200 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 4350 6150 4200
$Comp
L +BATT #PWR4
U 1 1 552FF1CB
P 6150 3400
F 0 "#PWR4" H 6150 3350 20  0001 C CNN
F 1 "+BATT" H 6150 3500 30  0000 C CNN
F 2 "" H 6150 3400 60  0000 C CNN
F 3 "" H 6150 3400 60  0000 C CNN
	1    6150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3500 6150 3400
Wire Wire Line
	5450 4750 5750 4750
Wire Wire Line
	5600 4850 5600 4750
Connection ~ 5600 4750
$Comp
L HEADER_3 U1
U 1 1 55301B33
P 4500 4750
F 0 "U1" H 4500 4550 60  0000 C CNN
F 1 "HEADER_3" H 4500 5000 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 4500 4750 60  0001 C CNN
F 3 "" H 4500 4750 60  0000 C CNN
	1    4500 4750
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR3
U 1 1 55301BC2
P 5600 5400
F 0 "#PWR3" H 5600 5400 30  0001 C CNN
F 1 "GND" H 5600 5330 30  0001 C CNN
F 2 "" H 5600 5400 60  0000 C CNN
F 3 "" H 5600 5400 60  0000 C CNN
	1    5600 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 5400 5600 5350
$Comp
L GND #PWR2
U 1 1 55301CB0
P 4800 4900
F 0 "#PWR2" H 4800 4900 30  0001 C CNN
F 1 "GND" H 4800 4830 30  0001 C CNN
F 2 "" H 4800 4900 60  0000 C CNN
F 3 "" H 4800 4900 60  0000 C CNN
	1    4800 4900
	1    0    0    -1  
$EndComp
$Comp
L +BATT #PWR1
U 1 1 55301CC7
P 4800 4600
F 0 "#PWR1" H 4800 4550 20  0001 C CNN
F 1 "+BATT" H 4800 4700 30  0000 C CNN
F 2 "" H 4800 4600 60  0000 C CNN
F 3 "" H 4800 4600 60  0000 C CNN
	1    4800 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4650 4800 4650
Wire Wire Line
	4800 4650 4800 4600
Wire Wire Line
	4750 4850 4800 4850
Wire Wire Line
	4800 4850 4800 4900
Wire Wire Line
	6150 3500 6400 3500
Wire Wire Line
	6400 3500 6400 3600
Wire Wire Line
	6150 4200 6400 4200
Wire Wire Line
	6400 4200 6400 4000
Wire Wire Line
	4750 4750 4950 4750
$Comp
L NMOS U2
U 1 1 5534587C
P 6100 4750
F 0 "U2" H 6250 4850 60  0000 C CNN
F 1 "BUK9832_55A" H 6600 4700 60  0000 C CNN
F 2 "cc3200_quad:SOT-223" H 6100 4750 60  0001 C CNN
F 3 "" H 6100 4750 60  0000 C CNN
	1    6100 4750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR5
U 1 1 552FEF4A
P 6250 5250
F 0 "#PWR5" H 6250 5250 30  0001 C CNN
F 1 "GND" H 6250 5180 30  0001 C CNN
F 2 "" H 6250 5250 60  0000 C CNN
F 3 "" H 6250 5250 60  0000 C CNN
	1    6250 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 5150 6150 5200
Wire Wire Line
	6150 5200 6250 5200
Wire Wire Line
	6250 5200 6250 5250
$EndSCHEMATC
